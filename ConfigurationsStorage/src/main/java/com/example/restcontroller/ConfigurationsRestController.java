package com.example.restcontroller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.entity.Configuration;
import com.example.repository.ConfigurationRepository;

@RestController
public class ConfigurationsRestController {
	static Logger log = LoggerFactory.getLogger(ConfigurationsRestController.class.getName());

	@Autowired
	ConfigurationRepository repository;

	@RequestMapping("/configurationsrest/{appName}")
	public @ResponseBody List<Configuration> configuration(@PathVariable String appName) {
		List<Configuration> confList = repository.findByAppNameAndIsActive(appName, true);

		if (log.isDebugEnabled()) {
			StringBuilder sb = new StringBuilder();

			for (Configuration conf : confList) {
				sb.append(conf.toString());
			}
			log.debug("Configurations will be sent for appName: " + appName + System.lineSeparator() + sb);
		}

		return confList;
	}

}
