package com.example.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.example.entity.Configuration;

public interface ConfigurationRepository extends CrudRepository<Configuration, Long> {
	public List<Configuration> findByValue(String value);

	public List<Configuration> findByIsActive(boolean isActive);

	public List<Configuration> findByAppName(String appName);

	public List<Configuration> findByAppNameAndIsActive(String appName, boolean isActive);

	public Configuration findByValue(String value, String appName);
	
    @Query("Select c from Configuration c where c.name like %:name%")
    public List<Configuration> findByNameContaining(@Param("name")String name);

}
