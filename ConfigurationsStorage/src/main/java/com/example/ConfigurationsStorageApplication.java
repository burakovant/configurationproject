package com.example;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.example.entity.Configuration;
import com.example.entity.Configuration.Type;
import com.example.repository.ConfigurationRepository;

@SpringBootApplication
public class ConfigurationsStorageApplication implements CommandLineRunner {

	@Autowired
	ConfigurationRepository configurationRepository;

	public static void main(String[] args) {
		SpringApplication.run(ConfigurationsStorageApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		List<Configuration> configurations = new ArrayList<Configuration>();

		// Add some initial configuration entities into repository.
		configurations.add(new Configuration("SiteName", Type.String, "trendyol.com", true, "SERVICE-A"));
		configurations.add(new Configuration("IsBasketEnabled", Type.Boolean, "true", true, "SERVICE-B"));
		configurations.add(new Configuration("MaxItemCount", Type.Integer, "50", false, "SERVICE-A"));
		configurations.add(new Configuration("IsBasketEnabled", Type.Boolean, "true", true, "SERVICE-A"));
		configurations.add(new Configuration("MaxItemCount", Type.Integer, "49", true, "SERVICE-A"));

		configurationRepository.save(configurations);
	}

}
