package com.example.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.entity.Configuration;
import com.example.entity.Configuration.Type;
import com.example.repository.ConfigurationRepository;

@Controller
public class ConfigurationsController {
	static Logger log = LoggerFactory.getLogger(ConfigurationsController.class.getName());

	@Autowired
	ConfigurationRepository repository;

	@RequestMapping("/configuration/{id}")
	public String configuration(@PathVariable Long id, Model model) {
		log.info("configuration");

		model.addAttribute("configuration", repository.findOne(id));
		return "configuration";
	}

	@RequestMapping(value = "/configurations", method = RequestMethod.GET)
	public String configurationsList(Model model) {
		model.addAttribute("configurations", repository.findAll());
		return "configurations";
	}

	@RequestMapping(value = "/configurations", method = RequestMethod.POST)
	public String configurationsAdd(@RequestParam String value, @RequestParam String name, @RequestParam Type type,
			Model model, String appName) {
		Configuration newconfiguration = new Configuration();
		newconfiguration.setName(name);
		newconfiguration.setType(type);
		newconfiguration.setValue(value);
		newconfiguration.setActive(true);
		newconfiguration.setAppName(appName);
		repository.save(newconfiguration);

		model.addAttribute("configuration", newconfiguration);
		return "redirect:/configurations/";
	}

	@RequestMapping(value = "/configurationUpdate/{id}", method = RequestMethod.GET)
	public String configurationsGetUpdate(@PathVariable Long id, Model model) {
		model.addAttribute("configuration", repository.findOne(id));
		log.info("redirecting to configurationUpdate");
		return "configurationUpdate";
	}

	@RequestMapping(value = "/configurationUpdate/{id}", method = RequestMethod.POST)
	public String configurationsUpdate(@PathVariable Long id, @RequestParam String name, @RequestParam Type type,
			@RequestParam String value, @RequestParam String isActive, Model model, String appName) {
		Configuration configuration = repository.findOne(id);
		log.info("configurationsUpdate");
		if (configuration != null) {
			configuration.setName(name);
			configuration.setType(type);
			configuration.setValue(value);
			configuration.setActive(true);
			configuration.setAppName(appName);
		}
		repository.save(configuration);

		model.addAttribute("configuration", configuration);
		return "redirect:/configurations/";
	}
}
