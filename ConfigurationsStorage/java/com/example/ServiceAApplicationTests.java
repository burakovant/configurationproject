package com.example;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.ConfigurationReader;
import com.example.ServiceAApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ServiceAApplication.class)

public class ServiceAApplicationTests {

	@Test
	public void testGetConfigs() {
		ConfigurationReader cr = new ConfigurationReader("SERVICE-A", "http://localhost:8080", 1000);
		String s = cr.getValue("SiteName", String.class);
		assertEquals("trendyol.com", s);
	}

}
