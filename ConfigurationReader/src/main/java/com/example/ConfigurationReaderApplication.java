package com.example;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConfigurationReaderApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(ConfigurationReaderApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		ConfigurationReader cr = new ConfigurationReader("SERVICE-A", "http://localhost:8080", 1000);
		String s = cr.getValue("SiteName", String.class);
	}

}
