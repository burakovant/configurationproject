package com.example;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.example.entity.Configuration;

public class ConfigurationReader {

	static Logger log = LoggerFactory.getLogger(ConfigurationReader.class.getName());

	private String appName;
	private String connectionString;
	private Map<String, String> innerCache;

	/**
	 * Initializes a new ConfigurationReader. If ConfigurationStorage goes down
	 * after initialization, it keeps last successfully readed values.
	 * 
	 * @param appName
	 *            Application Name that its configs are to be readed.
	 * @param connectionString
	 *            Connection url of ConnectionStorage application. i.e.
	 *            http(s)://www.example.org(:port)
	 * @param refreshTimerIntervalInMs
	 *            refresh Timer Interval in milliseconds.
	 */
	public ConfigurationReader(String appName, String connectionString, long refreshTimerIntervalInMs) {
		this.appName = appName;
		this.connectionString = connectionString;
		this.innerCache = new ConcurrentHashMap<String, String>();

		// schedule load cache from storage task at initialization.
		ScheduledExecutorService ses = Executors.newScheduledThreadPool(1);
		ses.scheduleAtFixedRate(runnableTask, 0, refreshTimerIntervalInMs, TimeUnit.MILLISECONDS);
	}

	Runnable runnableTask = new Runnable() {

		@Override
		public void run() {

			log.info("loadCache started");

			RestTemplate restTemplate = new RestTemplate();

			try {
				ResponseEntity<List<Configuration>> response = restTemplate.exchange(
						connectionString + "/configurationsrest/" + appName, HttpMethod.GET, null,
						new ParameterizedTypeReference<List<Configuration>>() {
						});
				List<Configuration> confList = response.getBody();

				StringBuilder sb = new StringBuilder();

				for (Configuration conf : confList) {
					innerCache.put(conf.getName(), conf.getValue());

					if (log.isDebugEnabled()) {
						sb.append(conf.toString());
					}
				}

				if (log.isDebugEnabled()) {
					log.debug("Configurations received from storage for appName: " + appName + System.lineSeparator()
							+ sb);
				}

				log.info("loadCache finished");

			} catch (RestClientException rce) {
				log.error("Communication error with Configuration Storage", rce);
			}

		}
	};

	/**
	 * 
	 * @param key
	 *            key to be accessed from configuration cache
	 * @param type
	 *            return type class of value
	 * @return value of the key
	 * @throws ClassCastException
	 *             if the value of the key is not null and is not assignable to the
	 *             type T.
	 */
	public <T> T getValue(String key, Class<T> type) {
		String value = innerCache.get(key);

		if (type.getSimpleName().equals("Double")) {
			return (T) Double.valueOf(value);
		} else if (type.getSimpleName().equals("Integer")) {
			return (T) Integer.valueOf(value);
		} else if (type.getSimpleName().equals("Boolean")) {
			return (T) Boolean.valueOf(value);
		} else {
			return (T) value;
		}
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getConnectionString() {
		return connectionString;
	}

	public void setConnectionString(String connectionString) {
		this.connectionString = connectionString;
	}
}
