# ConfigurationProject

ConfigurationStorage Storage kısmı için, ConfigurationReader configleri okuyabilmek için, SERVICE-A projesi de configi okuyacak örnek bir proje olması için geliştirilmiştir. Test edebilmek için ConfigurationStorage Spring Boot uygulamasını ayağa kaldırmak ve SERVICE-A uygulamasının test class'ı olan ServiceAApplicationTests kullanılabilir. Burada, üç tip configurasyon (String, Integer, Boolean) çekilip daha önce girilen değerle aynı olup olmadığı kontrol edilebilir. Ayrıca, http://localhost:8080/configurations üzerinden config yönetim ekranı kullanılabilir.

## ConfigurationStorage Project (Maven Artifact Id: configuration-storage)

Proje dökümantasyonundaki örnek değerler, uygulamanın Spring Booter'ı ayağa kalktığında default olarak repositorye Configuration Entity olarak eklenir. 

Uygulamanın web arayüzü, http://localhost:8080/configurations üzerinden çağırılabilir. Burada configurasyonlar gözlemlenir, güncellenir, eklenir ve filtrelenir.

Uygulamada ayrıca bir de Rest API mevcuttur. Bu API, her uygulamanın kendi configurasyonlarını çekebilmesini sağlar. Örneğin, SERVICE-A uygulaması için http://localhost:8080/configurationsrest/SERVICE-A. Diğer uygulamalar bu API'yi ConfigurationReader (Maven Artifact Id: configuration-reader) kütüphanesini kullanarak çağırabilirler.


## ConfigurationReader Project (Maven Artifact Id: configuration-reader)

ConfigurationReader objesi;

new ConfigurationReader(applicationName, connectionString, refreshTimerIntervalInMs);

şeklinde initialize ederek kullanılabilir. Obje initialize olduktan sonra, connectionString ile ConfigurationStorage uygulamasına bağlanır, verilen uygulama isminin configurasyonlarını çeker ve refreshInterval'e göre periyodik olarak tekrar bağlanıp configurasyonları günceller. Configurasyonları çekmek için Rest API kullanılmıştır. Periyodik güncelleme için ise java.util.concurrent kütüphanesinin ScheduledExecutorService'i kullanılmıştır.

Diğer uygulamalar bu API'yi ConfigurationReader (Maven Artifact Id: configuration-reader) kütüphanesini kullanarak çağırabilirler.