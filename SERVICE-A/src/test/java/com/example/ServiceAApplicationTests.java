package com.example;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ServiceAApplication.class)

public class ServiceAApplicationTests {

	@Test
	public void testGetStringSiteName() throws InterruptedException {
		ConfigurationReader cr = new ConfigurationReader("SERVICE-A", "http://localhost:8080", 1000);
		Thread.sleep(1000);

		String s = cr.getValue("SiteName", String.class);
		assertEquals("trendyol.com", s);
	}

	@Test
	public void testGetIntegerMaxItemCount() throws InterruptedException {
		ConfigurationReader cr = new ConfigurationReader("SERVICE-A", "http://localhost:8080", 1000);
		Thread.sleep(1000);

		Integer count = cr.getValue("MaxItemCount", Integer.class);
		assertEquals(Integer.valueOf(49), count);
	}

	@Test
	public void testGetBooleanIsEnabled() throws InterruptedException {
		ConfigurationReader cr = new ConfigurationReader("SERVICE-A", "http://localhost:8080", 1000);
		Thread.sleep(1000);

		Boolean isBasketEnabled = cr.getValue("IsBasketEnabled", Boolean.class);
		assertEquals(true, isBasketEnabled);
	}
}
